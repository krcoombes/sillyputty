---
author: Kevin R, Coombes
date: 9 November 2018
title: Apoptosis Projects
---

# Overview
We currently have two active projects that use TCGA RNA-Seq data on the
subset of genes related to apoptosis. Both projects are represented
here, with separate subfolders in the `doc/paper` folder.

## Thresher
One of our early ideas for applicatiosn of Thresher was to decompose
the apoptosis pathway into "_pathways building blocks_" (PBBs) or
"_biological components_". We use those terms interchangably to refer
to our replacement of the mathematical principal components with
something that is biologically interpretable.

## SillyPutty
While trying to repeat/refine/improve Min Wang's original application
fo Thresher to the apoptosis data set, I had an idea for a new
clusteing algorithm (currently called `SillyPutty`). The first
applications of the method are to understand the apoptosis data, and
we expect that this will remain a significant application when we
eventually get around to publishing the method.

# Changes

This folder used to contain a subfolder called "Papers". It's only
content was a published paper being used as a reference for our own
manuscript. We move that PDf file into the ./doc/background folder
instead, and deleted the unneeded (and confusing) top level "Papers"
folder.

It also used to contain a subfolder called "Mercator", whoch comntaind
the Word version of our published paper on the mercator R
package, along with an EndNote library. We move the Word document into
a the cytogps Git Project. We deleted the EndNote library, since it
has already been ported over to Zotero.
