---
title: "Make Seed Set"
author: "Kevin R. Coombes"
date: "17 August 2023"
output:
  html_document: default
  pdf_document: default
params:
  mySeed: 47224
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_chunk$set(time_it = TRUE)
```

```{r}
library(Umpire)
suppressMessages( library(Mercator) )
```

# Background
We want to simulate data using the `Umpire` R package. We already have a base set
of scripts in the folder "Final", We will create multiple subfolders with customized
copies of the scripts. By "customization, we mean the following. Each script uses
the "params" idea from "knitr" to set a value for "mySeed", which is used to set a
different starting seed for each collection of simulations. We will change those
seed parameters so that each collection is reproducible, yet different.

The purpose of this particular report is to generate our seed sets.

# Randomizing Starting Seeds

```{r seeds}
NReps <- 30 # upper bound on the number of replicate simulation runs
set.seed(81085)
raw <- abs(rnorm(4*NReps, 0, 1))
shift1 <- 100*raw
frac1 <- shift1 - trunc(shift1)
fiveDigits <- matrix(round(1E5 * frac1), ncol = 4)
colnames(fiveDigits) <- c("simulate", "cluster", "evaluate", "summarize")
fiveDigits
```

```{r savor}
write.table(fiveDigits, file = "mySeeds.tsv", quote=FALSE, sep = "\t", row.names = FALSE)
```


```{r si}
sessionInfo()
```
