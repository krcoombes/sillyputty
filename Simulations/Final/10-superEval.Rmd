---
title: "Summarizing Results"
author: "Dwayne Tally, Polina Bombina, Zachary B. Abrams, and Kevin R. Coombes"
date: "25 August 2023"
output:
  html_document: default
  pdf_document: default
params:
  mySeed: 4876
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(fig.width = 8, fig.height = 12)
options(width = 96)
```

# Overview
In this report, we want to summarize the results we obtained over the complete set of
simulations.

We start by loading some useful library packages.
```{r libs}
library(beanplot)
library(viridis)
library(Polychrome)
data(Dark24)
pal <- Dark24[c(1, 4, 3, 9, 8, 11, 5, 7, 2, 10, 14, 15, 12:13, 16:24, 6)]
lpal <- as.list(pal)
```

Now we begin to accumulate results by reading in the findings from our very first set
of simulations.
```{r firstSim}
load("evalBundle.Rda") 
bigBundle <- bundle
bigBundle$Rep <- "Simrep00"
bigTimings <- list(Simrep00 = timings)
```

Next, we sweep across the subfolders containing different simulation sets.
```{r upsweep}
SR <- dir(pattern="Simrep")
tenv <- new.env()
for (D in SR) {
  target <- file.path(D, "evalBundle.Rda")
  if (!file.exists(target)) {
    next
  }
  load(target, env = tenv)
  bundle <- get("bundle", tenv)
  bundle$Rep <- D
  bigBundle <- rbind(bigBundle, bundle)
  rm(bundle)
  bigTimings[[D]] <- get("timings", tenv)
}
rm(tenv)
```

We want to make sure that all of the appraent "character" vectors are actually R factors
with reasonable ordering to the "levels".
```{r leveler}
bigBundle$Rep <- factor(sub("Sim", "", bigBundle$Rep))
bigBundle$Clusters <- factor(as.character(bigBundle$Clusters),
                             levels = c("3k", "6k", "12k"))
bigBundle$Noise <- factor(as.character(bigBundle$Noise),
                          levels = c("L", "M", "H"))
bigBundle$Method <- factor(as.character(bigBundle$Method),
                          levels = c("CLARA", "CLARA+Silly",
                                     "PAM","PAM+Silly",
                                     "Spectral", "Spec+Silly",
                                     "Hierarchical","Hier+Silly",
                                     "Kmeans", "Kmeans+Silly",
                                     "Subspace", "Sub+Silly",
                                     "SillyPutty", "Truth"))
summary(bigBundle)
```

While the `Method` and `Clusters` factor are completely balanced, some of the other
factors are not. We did fewer simulations using high noise with fewer samples and features,
by design.

# Timings
Now we want to look at how long it took for each method. The values we recorded are for
running each method on a complet set of 27 different combinations of simulation parameters.
```{r timer}
ltim <- sapply(bigTimings, function(timelist) {
  sapply(timelist, function(TL) {
    difftime(TL["ET"], TL["ST"], units = "secs")
  })
})
rownames(ltim) <- sub("\\.ET", "", rownames(ltim))
colnames(ltim) <- sub("Sim", "", colnames(ltim))
apply(ltim, 2, mean) # basically the same
```
The first set, `rep00`, was run by itself on a different machine, as a test that the
simulation code was error-free. Replicates `rep01` through `rep06` were run simultaneously
on a multi-core machine with 96 GB of RAM. Replicates `rep07` through `rep12` were also
run on that machine at a different time,

```{r showTimes, fig.height=5, fig.width=11}
barplot(sort(apply(ltim, 1, mean)), ylab = "Seconds")
boxplot(t(ltim))
```

# Preparing Visualizations
For some display purposes, we want to restructure the evaluation data from "long"
vector columns to "wide" matrices.
```{r matrize}
simnames <- apply(bigBundle[,5:9], 1, paste, collapse = "|")
attach(bigBundle)
ari <- tapply(ARI, list(simnames, Method), mean)
sw  <- tapply(SW, list(simnames, Method), mean)
wss <- tapply(WSS, list(simnames, Method), mean)
detach()
```

Because the baseline within-group sum of squares is different in the "true" classifications
for each sample set, we are going to normalize the values for each simulation by dividing
by the known true value.
```{r NormWSS}
NormWSS <- sweep(wss, 1, wss[, "Truth"], "/")
nSim <- nrow(wss)
NormWSS[, "Truth"] <- jitter(rep(1, nSim), amount = 0.0001)
bigBundle$NormWSS <- as.vector(NormWSS) 
bigBundle$ARI[bigBundle$Method == "Truth"] <- jitter(rep(1, nSim), amount = 0.0001)
```

## Achieving Truth
Here we report the number of times (out of `r nrow(ari)` simulations) that a method
correctly classifies all samples in a simulated data set.
```{r justari}
sort(dwayne <- apply(ari, 2, function(x) sum(x == 1)))
sort(round(100*dwayne/nrow(ari)))
```

## Heatmaps
Here we present each of the outcome summaries as heatmaps, where rows are the separate
simulations and columns are methods.
```{r fig.cap = "Heatmaps", fig.width=6, fig.height=6}
colormap <- viridis(64)
heatmap(ari, scale = "none", col = colormap, main = "Adjusted Rand Index")
heatmap(sw, scale = "none", col = colormap, main = "Silhouette Width")
heatmap(NormWSS, scale = "none", col = colormap, main = "Normalized Within Group SS")
heatmap(wss, scale = "row", col = colormap, main = "Within Group SS (Scaled)")
```

## Bean Plots
```{r entropy}
load("entropies.Rda")
meanEnt <- apply(AE, c(1, 3), mean, na.rm=TRUE)
meanEnt <- meanEnt[, c(1:2, 7:8, 11, 10, 3:4, 5:6, 13, 12, 9, 14)]
data.frame(M=colnames(meanEnt),
           B=levels(bigBundle$Method))
colnames(meanEnt) <- levels(bigBundle$Method)
meth <- factor(rep(colnames(meanEnt), each = nrow(meanEnt)), 
                   levels = levels(bigBundle$Method))
ment <- as.vector(meanEnt)
```

```{r multiplot, fig.cap="Bean plots.", fig.width=7, fig.height=8}
opar <- par(mfrow = c(4,1))
beanplot(ARI ~ Method, data = bigBundle, what = c(1, 1, 1, 0),
         cutmin = -1, cutmax = 1,
         main = "Adjusted Rand Index", col = lpal, las=2, xaxt="n")
text(x = 1:14, y = par("usr")[3] - 0.1, adj = 1, labels = colnames(meanEnt),
     srt = 35, xpd = TRUE)
mtext("A", side =2, las = 2, at = 1.3, cex = 1.2, line = 2)
beanplot(SW ~ Method, data = bigBundle, what = c(1, 1, 1, 0),
         cutmin = -1, cutmax = 1,
         main = "Silhouette Width", col = lpal, las=2, xaxt = "n")
text(x = 1:14, y = par("usr")[3] - 0.01, adj = 1, labels = colnames(meanEnt),
     srt = 35, xpd = TRUE)
mtext("B", side =2, las = 2, at = 0.16, cex = 1.2, line = 2)
beanplot(NormWSS ~ Method, data = bigBundle, what = c(1, 1, 1, 0),
         bw = "nrd0", cutmin = 1, log = "",
         main = "Normalized WSS", col = lpal, las=2, xaxt = "n")
text(x = 1:14, y = par("usr")[3] - 0.01, adj = 1, labels = colnames(meanEnt),
     srt = 35, xpd = TRUE)
mtext("C", side =2, las = 2, at = 1.13, cex = 1.2, line = 2)
beanplot(ment ~ meth, what = c(1,1,1,0), 
         main = "Entropy", bw = "nrd0", col = lpal, las = 2,
         cutmin=0, cutmax = 1, xaxt = "n")
text(x = 1:14, y = par("usr")[3] - 0.1, adj = 1, labels = colnames(meanEnt),
     srt = 35, xpd = TRUE)
mtext("D", side =2, las = 2, at = 1.3, cex = 1.2, line = 2)
par(opar)
```

```{r pngmulti}
resn = 300
png("multiplot.png", width = 8*resn, height = 7*resn, res = resn,
    bg = "white", pointsize = 9)
<<multiplot>>
dev.off()
```

```{r pdfmulti}
pdf("multiplot.pdf", width = 8, height = 7,
    bg = "white", pointsize = 9)
<<multiplot>>
dev.off()
```

```{r fig1Abstract}
resn <- 300
png(file = "OverallARI.png", width = 14*resn, height = 4*resn, res = resn,
    bg = "white", pointsize = 10)
beanplot(ARI ~ Method, data = bigBundle, what = c(1, 1, 1, 0),
         cutmin = 0, cutmax = 1,
         main = "Adjusted Rand Index", col = lpal)
dev.off()
sort(round(apply(ari, 2, mean), 2))
```
```{r}
resn <- 300
png(file = "OverallSW.png", width = 14*resn, height = 4*resn, res = resn,
    bg = "white", pointsize = 10)
beanplot(SW ~ Method, data = bigBundle, what = c(1, 1, 1, 0),
         cutmin = 0, cutmax = 1,
         main = "Silhouette Width", col = lpal)
dev.off()

```


```{r}
resn <- 300
png(file = "OverallNWSS.png", width = 14*resn, height = 4*resn, res = resn,
    bg = "white", pointsize = 10)
beanplot(NormWSS ~ Method, data = bigBundle, what = c(1, 1, 1, 0), bw = "nrd0",
         cutmin = 1, log = "",
         main = "Normalized WSS", col = lpal)
dev.off()

```




Both the heatmaps and bean plots show that the normalized WSS (`NormWSS`) is probably a
more useful comparison metric than the unadjusted WSS. They also show that the methods
PAM and CLARA are far inferior to other methods, while Spectral and Hierarchical have
intermediate performance. In general, following almost any method by applying SillyPutty
makes the results substantially better.


# Other Factors

## Replicate Simulations
There are essentially no differences in performance metrics between simulation sets.
```{r arireps, fig.width=10, fig.height=5}
beanplot(ARI ~ Rep, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal)
beanplot(SW ~ Rep, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal)
beanplot(NormWSS ~ Rep, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal, log = "", bw = "nrd0")
```

## ARI
Now we look at how various simulation parameters affect the Adjusted Rand Index.
Basically:

* More clusters decrease ARI. (There are more ways to misclassify a sample.)
* Higher noise levels decrease ARI.
* More samples decrease ARI. (There are more samples to misclassify.)
* More features decfrease ARI. (Extra features consist entirely of irrelevant
  measures unrelated to true subtypes.)
* All factors appear top be independent.

```{r arimix, fig.width=8, fig.height=5}
beanplot(ARI ~ Clusters, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:3])
beanplot(ARI ~ Noise, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:3])

beanplot(ARI ~ Samples, data = bigBundle, 
         what = c(1, 1, 1, 0), col = lpal[1:3])
beanplot(ARI ~ Features, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:3])

beanplot(ARI ~ Samples + Features, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:3])
beanplot(ARI ~ Noise + Features, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:6])
beanplot(ARI ~ Clusters + Features, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:6])

beanplot(ARI ~ Noise + Samples, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:6])
beanplot(ARI ~ Clusters + Samples, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:6])

```

```{r arieffect, fig.width =8, fig.height = 6}
opar <- par(mfrow = c(2,2),mai = c(1.02, 0.82, 0.32, 0.12))
beanplot(ARI ~ Clusters, data = bigBundle,
         cutmin = -1, cutmax = 1,
         what = c(1, 1, 1, 0), col = lpal[1:3],
         xlab = "Number of Clusters")
mtext("A", side = 2, at = 1.05, line = 2, las=1, cex = 1.2)
beanplot(ARI ~ Samples, data = bigBundle, 
         cutmin = -1, cutmax = 1,
         what = c(1, 1, 1, 0), col = lpal[1:3],
         xlab = "Number of Samples")
mtext("B", side = 2, at = 1.05, line = 2, las=1, cex = 1.2)
beanplot(ARI ~ Features, data = bigBundle,
         cutmin = -1, cutmax = 1,
         what = c(1, 1, 1, 0), col = lpal[1:3],
         xlab = "Number of Features")
mtext("C", side = 2, at = 1.05, line = 2, las=1, cex = 1.2)
beanplot(ARI ~ Noise, data = bigBundle,
         cutmin = -1, cutmax = 1,
         what = c(1, 1, 1, 0), col = lpal[1:3],
         xlab = "Noise Level")
mtext("D", side = 2, at = 1.05, line = 2, las=1, cex = 1.2)
par(opar)
```

```{r pngari}
resn <- 300
png(file="arieffect.png", width = 8*resn, height = 6*resn, res = resn, bg = "white")
<<arieffect>>
dev.off()
```

```{r pdfari}
pdf(file="arieffect.pdf", width = 8, height = 6, bg = "white")
<<arieffect>>
dev.off()
```

## Silhouette Width
We make a similar set of plots for the effects of factors on the silhouette width.
These are similar to the ARI results.
```{r swmix, fig.width=8, fig.height=5}
beanplot(SW ~ Clusters, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:3])
beanplot(SW ~ Noise, data = bigBundle, 
         what = c(1, 1, 1, 0), col = lpal[1:3])
beanplot(SW ~ Samples, data = bigBundle, 
         what = c(1, 1, 1, 0), col = lpal[1:3])
beanplot(SW ~ Features, data = bigBundle, 
         what = c(1, 1, 1, 0), col = lpal[1:3])

beanplot(SW ~ Samples + Features, data = bigBundle, 
         what = c(1, 1, 1, 0), col = lpal[1:3])
beanplot(SW ~ Noise + Features, data = bigBundle, 
         what = c(1, 1, 1, 0), col = lpal[1:6])
beanplot(SW ~ Clusters + Features, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:6])

beanplot(SW ~ Noise + Samples, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:6])
beanplot(SW ~ Clusters + Samples, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:6])

```

# Normalized WSS
```{r wssmix, fig.width=8, fig.height=5}
beanplot(NormWSS ~ Clusters, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:3])
beanplot(NormWSS ~ Noise, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:3])
beanplot(NormWSS ~ Samples, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:3])
beanplot(NormWSS ~ Features, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:3])

beanplot(NormWSS ~ Samples + Features, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:3])
beanplot(NormWSS ~ Noise + Features, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:6])
beanplot(NormWSS ~ Clusters + Features, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:6])

beanplot(NormWSS ~ Noise + Samples, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:6])
beanplot(NormWSS ~ Clusters + Samples, data = bigBundle,
         what = c(1, 1, 1, 0), col = lpal[1:6])

```


# Appendix

```{r si}
sessionInfo()
```
