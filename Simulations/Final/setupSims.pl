#! perl -w

use strict;
use warnings;
use File::Copy;

my %seeds = (
    simulate => 82625,
    cluster => 17272,
    evaluate => 4876,
    summarize => 75488
    );

my %files = (
    simulate => "01-Simulate-Rep00.Rmd",
    cluster => "02-Cluster-Rep00.Rmd",
    evaluate => "03-Evaluate-Rep00.Rmd",
    summarize => "04-Summarize-Rep00.Rmd"
    );

my $seedset = "mySeeds.tsv";
open(SRC, "<$seedset") or die "Cannot open '$seedset': $!\n";
my $line = <SRC>;
chomp($line);
my @keys = split("\t", $line);
print STDERR join("\t", @keys), "\n";
my $counter = 0;
while($line = <SRC>) {
    $counter++;
    my $tag = ($counter < 10 ? "0$counter" : "$counter");
    my $folder = "Simrep$tag";
    mkdir $folder or die "Cannot create '$folder': $!\n";
    print STDERR "Created $folder\n";
    copy("fake.R", $folder) or die "Unable to copy 'fake.R': $!\n";

    my $fname = "00-doFullSim.Rmd";
    my $document = do {
	local $/ = undef;
	open my $fh, "<", $fname
	    or die "could not open $fname: $!\n";
	<$fh>;
    };
    $document =~ s/00/$tag/g;
    my $outgo = "$folder/$fname";
    print STDERR "\tWriting '$outgo'\n";
    open(TGT, ">$outgo") or die "Cannot create '$outgo': $!\n";
    print TGT $document;
    close(TGT);

    chomp $line;
    my @gunk = split("\t", $line); 
    my %newseed = ();
    foreach my $dex (0 .. $#keys) {
	$newseed{$keys[$dex]} = $gunk[$dex];
    }

    foreach my $k (@keys) {
	my $fname = $files{$k};
	my $document = do {
	    local $/ = undef;
	    open my $fh, "<", $fname
		or die "could not open $fname: $!\n";
	    <$fh>;
	};
	$document =~ s/$seeds{$k}/$newseed{$k}/;
	$fname =~ s/00/$tag/;
	my $outname = "$folder/$fname";
	print STDERR "\tWriting '$outname'\n";
	open(TGT, ">$outname") or die "Cannot create '$outname': $!\n";
	print TGT $document;
	close(TGT)
    }
}
close(SRC);

exit;
__END__
