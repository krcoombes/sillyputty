---
title: "Overview of Simulations"
author: 'Kevin R. Coombes"
date: "9 August 2023"
---

# Overview
The purpose of this file is to try to document the simulations
performed by Dweayne Tally, whise results were stored in a Box
folder.

# Old Code folder
We copied the `Code` folder from the `Box` project called
`SillyPuttySimulations`. It cnotained three files, which we start by
describing here.

## Range Silly Putty
There are two scripts containing early implementations of the function
`RangeSillyPutty`. These files have no external inputs (files to read
or load) nor outputs (files to write or save). We have now
incorporated the function into the `SillyPutty` R package under the
name `findClusterNumber`. As a result, these files are no longer
needed.

## Silly Putty Averages
There is an Rmarkdown script called `SillyPuttyAverages.Rmd`. It
doesn't write or save any files. It does load a bunch of different
things, with the goal of producing tables of average results. (It is
unclear why these result tables are not saved by the script.)

# Iteration Folders
The Box project contains 14 identically structured subfolders, each
arising from one run of simulations across the entire spectrum of
parmaters and clustering algorithmas. Eaxch folder contains 2
Rmarkdown (Rmd) files and many binary R Data (Rda) files.

## All Engines
The `All_engines_#.Rmd` script has no inputs (files to read or
load). It creates lots of binary Rda files as outputs, which will be
documented later.

Based on comments in the code, there are blocks for the following:

* First Engine 3k, noise high, 1000 samples, 10k features
* Second Engine 3k, noise medium, 1000 samples, 10k features
* third Engine 3k, noise low, 1000 samples, 10k features
* fourth Engine 3k, noise medium, 600 samples, 10k features
* Fifth Engine 3k, noise low, 600 samples, 10k features
* Sixth Engine 3k, noise medium, 1000 samples, 5k features
* Seventh Engine 3k, noise low, 1000 samples, 5k features
* eigth Engine 3k, noise medium, 600 samples, 5k features
* ninth Engine 3k, noise low, 600 samples, 5k features
* First Engine 3k, noise high, 1000 samples, 10k features
* Second Engine 6k, noise medium, 1000 samples, 10k features
* third Engine 6k, noise low, 1000 samples, 10k features
* fourth Engine 6k, noise medium, 600 samples, 10k features
* Fifth Engine 6k, noise low, 600 samples, 10k features
* Sixth Engine 6k, noise medium, 1000 samples, 5k features
* Seventh Engine 6k, noise low, 1000 samples, 5k features
* eigth Engine 6k, noise medium, 600 samples, 5k features
* ninth Engine 6k, noise low, 600 samples, 5k features
* First Engine 12k, noise high, 1000 samples, 10k features
* Second Engine 12k, noise medium, 1000 samples, 10k features
* third Engine 12k, noise low, 1000 samples, 10k features
* fourth Engine 12k, noise medium, 600 samples, 10k features
* Fifth Engine 12k, noise low, 600 samples, 10k features
* Sixth Engine 12k, noise medium, 1000 samples, 5k features
* Seventh Engine 12k, noise low, 1000 samples, 5k features
* eigth Engine 12k, noise medium, 600 samples, 5k features
* ninth Engine 12k, noise low, 600 samples, 5k features

**Editorial Comment:** The code includes compete copies of the same
thing over-and-over again, then m ildly edited. It would benefit
greatly from defining a function once to do these steps and invoking
that function with different parameters.

Each of the code blocks starts by using the `Umpire` package to create
a `CancerEngine`. The varying parameters are:

* nPattern = 3, 6, or 12. Each "pattern" corresponds to a subtype or
  cluster.
* (unnamed parameter to `rand`) number of samples = 1000 or 600.
* (unnamed parameter to `[`) number of features = 10000 or 5000

Surprisingly, the parameters to the `SpercialNoise` function all
appear to be the same. Maybe I am missing something, or maybe this is
getting modified later.

### Outputs

* `all_engines#.Rda`: contains 27 simulated data sets.
* `all_labels#.Rda`: contains the true cluster labels for each
  simulated data set.
* `all_engines_transposed#.Rda`: for some reason, contains transposed
  versions of all of the simulated data sets.
* `all_engines_dis#.Rda`: Contains Euclidean distance matrices between
  samples in all simulated data sets.

## All Engines One Iteration Rep

### Inputs
Loads the distance matrices (`all_engines_dis`) and true cluster
labels (`all_labels`) for all simulated data sets.

### Outputs

* `mercator_all_engines#.Rda`: Contains `Mercator` objects with
  visualizations for all simulated data sets.
* `truebin_all_engines#.Rda`: Contains Mercator objects with
  clustering manually set to the true, known cluster labels.
* `Si.Truth_all_engines#.Rda`:
* `ari.Truth_all_engines#.Rda`:
* `ARI_Pam_all_engines#.Rda`:
* `SS_Pam_all_engines#.Rda`:
* `SI_Pam_all_engines#.Rda`:
* `SS_Hier_all_engines#.Rda`:
* `SI_Hierarchical_all_engines#.Rda`:
* `ARI_Hierarchical_all_engines#.Rda`:
* `SS_HierSilly_all_engines#.Rda`:
* `SI_hierSilly_all_engines#.Rda`:
* `ARI_hierSilly_all_engines#.Rda`:
* `SS_Spec_all_engines#.Rda`:
* `SI_Spec_all_engines#.Rda`:
* `ARI_Spec_all_engines#.Rda`:
* `SS_subSpace_all_engines#.Rda`:
* `SI_sb_all_engines#.Rda`:
* `ARI_sb_all_engines#.Rda`:
* `SS_Clara_all_engines#.Rda`:
* `SI_cl_all_engines#.Rda`:
* `ARI_cl_all_engines#.Rda`:
* `SS_kMeans_all_engines#.Rda`:
* `SI_km_all_engines#.Rda`:
* `ARI_km_all_engines#.Rda`:
* `SS_sillyKMeans_all_engines#.Rda`:
* `SI_kmSilly_all_engines#.Rda`:
* `ARI_kmSilly_all_engines#.Rda`:
* `SS_sillyPutty_all_engines#.Rda`:
* `ARI_Silly_all_engines#.Rda`:
* `SI_Silly_all_engines#.Rda`:
