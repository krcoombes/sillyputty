---
title: "Test Noise Levels"
author: "Dwayne Tally, Polina Bombina, And Kevin R. Coombes"
date: "3/17/2022"
output:
  html_document: default
  pdf_document: default
params:
  mySeed: 47224
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_chunk$set(time_it = TRUE)
```

```{r}
library(Umpire)
suppressMessages( library(Mercator) )
```

# Background
We want to test the effect of noise levels on the PCA/MDS displays of data
simulated using the `Umpire` R package.

# Simulating Data
Making the cancer engines. Here is a script to make one Engine and generate 
a single noise-free data set using that engine. (**Note** "Engine" is the
colloquial term used in the `Umpire` pacakge for a "random vector generator".

```{r myEngines}
set.seed(params$mySeed)
mySimulation <- function(NTypes, sampSize, featSize = NULL) {
  # Underlying Survival Model (not used)
  sm <- SurvivalModel(baseHazard=1/5, accrual=5, followUp=1)
  ## Build a CancerModel with N subtypes
  nBlocks <- 20  # this can NOT equal 5 unless you change the default HIT function (5 at a time)
  cm <- CancerModel(name = "cansim", # don't care
                    nPossible = nBlocks,
                    nPattern = NTypes, #number of subtypes, or clusters
                    OUT = function(n) rnorm(n, 0, 1), 
                    SURV= function(n) rnorm(n, 0, 1),
                    survivalModel=sm)  
  nTotalBlocks <- nBlocks + 100 ## Include 100 unafected blocks/pathways
  ## Assign values to hyperparameters
  blockSize <- round(rnorm(nTotalBlocks, 100, 30))
  ## log normal mean hyperparameters
  mu0    <- 6
  sigma0 <- 1.5
  ## log normal sigma hyperparameters
  rate   <- 28.11
  shape  <- 44.25
  ## within block correlation
  p <- 0.6
  w <- 5
  ## Set up the baseline Engine
  rho <- rbeta(nTotalBlocks, p*w, (1-p)*w)
  base <- lapply(1:nTotalBlocks, function(i) {
    bs <- blockSize[i]
    co <- matrix(rho[i], nrow=bs, ncol=bs)
    diag(co) <- 1
    mu <- rnorm(bs, mu0, sigma0)
    sigma <- matrix(1/rgamma(bs, rate=rate, shape=shape), nrow = 1)
    covo <- co *(t(sigma) %*% sigma)
    MVN(mu, covo)
  })
  eng <- Engine(base)
  ## Alter the means if there is a hit
  altered <- alterMean(eng, normalOffset, delta = 0, sigma = 1)
  ## Build the CancerEngine
  object <-  CancerEngine(cm, eng, altered)
  ## Simulate the data
  dset <- rand(object, sampSize, keepall = TRUE)
  labels <- dset$clinical$CancerSubType
  d1 <- dset$data
  if (!is.null(featSize) && featSize > 1000) { # magic lower bound
    d1 <- d1[featSize,]
  }
  dimnames(d1) <- list(paste("Feat", 1:nrow(d1), sep=''),
                       paste("Samp", 1:ncol(d1), sep=''))
  names(labels) <- colnames(d1)
  list(engine = object, # save the cancer engine
       dataset = d1,    # save the simulated data set
       labels = labels) # save the cluster labels
}
```

## Building an Engine (NumClust, NumSamples, NumFeatures)
Now we build a single engine, and generate one noise-free data set.
```{r fig.cap="Noise-free simulated data with 3 clusters."}
set.seed(61593)
sim1  <- mySimulation(3,  300) # creates an engine and a data set, with cluster labels
raw <- sim1$dataset
dmat <- dist(t(raw))
merc <- Mercator(dmat, "done", "hclust", 3)
plot(merc, main = paste("noise level:", 0))
merc <- addVisualization(merc, "mds")
plot(merc, view = "mds", main = paste("noise level:", 0))
```

## Noise
We want to take the same (underlying biologically true) data set and apply different
levels of noise. To understand how this idea works, we first look at the `NoiseModel`
approach:
```{r nm}
NoiseModel
```
The data we save for a noise model makes it possible to incorporate both additive noise
($\epsilon \sim N(\nu. \tau)$) and multiplicative noise ($\eta \sim N(0, \phi)$).
This idea was originally motivated by looking at gene expression data with microarrays,
where the data was collected on the "raw-linear" scale, Additive noise came from
cross-hybridization and multiplicative  noise came from the gain on the fluorescence
detectors. We apply the noise using the `blur` function:
```{r blur}
getMethod("blur", "NoiseModel")
```
The key point here is that we transform the raw data by $x \mapsto e^\eta x + \epsilon$.
However, by default, `Umpire` generates data on the log-transformed scale. And, of course,
if the additive noise on the linear scale is small, then we approximately have
$\log(x) \mapsto \eta + \log(x)$. So, additive noise on the log scale is more like
multiplicative noise on the linear scale. Meanwhile, multiplicative noise on the log scale
has no natural physical or biological interpretation. So, we only want to use additive
noise here.

Now the *scale* of the additive noise (i.e., its standard deviation) is more important for
what we are doing than its mean. (You can convince yourself of this by computing noise as 
normal N(varying, fixed) and adding it to the raw data as we do below.) Since we are using
a gamma distribution to model differing standard deviations $\tau$, that means we have to
work with the mean $\mu$ of the gamma-standard-deviation, which equals "shape" times "scale".
We are going to fix the shape parameter $\alpha$ and set scale $=\mu/\alpha$ to vary
the noise level.
```{r addingNoise}
SpecialNoise <- function(nFeat, nu = 0.1, shape = 1.02, scale = 0.05/shape) {
  NoiseModel(nu = nu,
             tau = rgamma(nFeat, shape = shape, scale = scale),
             phi = 0)
}

noiseLevels <- c(0.001, 0.01, 0.03, 0.05, 0.1, 0.3, 0.5, 1, 2, 3, 5, 10)
fuzz <- lapply(noiseLevels, function(N) {
  nm <- SpecialNoise(nrow(raw), nu = 0.1, shape = 1.02, scale = N/1.02)
  d1 <- blur(nm, raw)
  dmat <- dist(t(d1))
  merc <- Mercator(dmat, "done", "mds", 3)
  plot(merc, main = paste("noise level:", N))
})
```


```{r si}
sessionInfo()
```
