---
title: "KCSillyPuttySimDataWithNoise6K50nB1000S"
author: "Dwayne Tally"
date: "3/17/2022"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
library(Umpire)
library(SillyPutty)
library(cluster)
library(stats)
library(mclust)
library(Mercator)
```
#Generating synthetic Data
Making the cancerEngine
```{r}
showClass("CancerEngine")
set.seed(21315)
## Set up survival outcome; baseline is exponential
sm <- SurvivalModel(baseHazard=1/5, accrual=5, followUp=1)
## Build a CancerModel with 6 subtypes
nBlocks <- 50    # number of possible hits 5/20/50
cm <- CancerModel(name="cansim",
                  nPossible=nBlocks,
                  nPattern=6,
                  OUT = function(n) rnorm(n, 0, 1), 
                  SURV= function(n) rnorm(n, 0, 1),
                  survivalModel=sm)
## Include 100 blocks/pathways that are not hit by cancer
nTotalBlocks <- nBlocks + 100
## Assign values to hyperparameters
## block size
blockSize <- round(rnorm(nTotalBlocks, 100, 30))
## log normal mean hypers
mu0    <- 6
sigma0 <- 1.5
## log normal sigma hypers
rate   <- 28.11
shape  <- 44.25
## block corr
p <- 0.6
w <- 5
## Set up the baseline Engine
rho <- rbeta(nTotalBlocks, p*w, (1-p)*w)
base <- lapply(1:nTotalBlocks,
               function(i) {
                 bs <- blockSize[i]
                 co <- matrix(rho[i], nrow=bs, ncol=bs)
                 diag(co) <- 1
                 mu <- rnorm(bs, mu0, sigma0)
                 sigma <- matrix(1/rgamma(bs, rate=rate, shape=shape), nrow=1)
                 covo <- co *(t(sigma) %*% sigma)
                 MVN(mu, covo)
               })
eng <- Engine(base)
## Alter the means if there is a hit
altered <- alterMean(eng, normalOffset, delta=0, sigma=1)
## Build the CancerEngine using character strings
object <- CancerEngine(cm, "eng", "altered")
## Or build it using the actual Engine components
ob <- CancerEngine(cm, eng, altered)
summary(object)
summary(ob)
```
Generating simulated data and extracting the cancer sub types
First Experiment generating 240 samples
```{r}
set.seed(111544)
## Simulate the data
#original sample size 1000
dset <- rand(object, 1000, keepall = TRUE)
summary(dset$clinical)
summary(dset$data[, 1:3])
labels <- dset$clinical$CancerSubType
d1 <- dset$data
class(d1)
mu <- apply(d1, 1, mean)
summary(mu) # note that this is log-scale data. 
```
#Noise
Adding Noise model 
```{r eval = FALSE}
SpecialNoise <- function(nFeat, nu = 0.1, shape = 1.02, scale = 0.05/shape) {
  NoiseModel(nu = nu,
             tau = rgamma(nFeat, shape = shape, scale = scale),
             phi = 0)
}
nm <- SpecialNoise(nrow(d1))
d1 <- blur(nm, d1)
#summary(d1)
```
Need to transpose the dataset so that the number of rows (Samples) matches the total number in labels
```{r}
set.seed(22215)
tdis <- t(d1)
dimnames(tdis) <- list(paste("Sample", 1:nrow(tdis), sep=''),
                     paste("Feature", 1:ncol(tdis), sep=''))
dis <- dist(tdis)
names(labels) <- rownames(dis)
```
#Generating plots,silhouette widths, and adjusted rand index
Function obtain from newman GI to use mercator to visualize the data
```{r}
mercViews <- function(object, main, tag = NULL) {
  opar <- par(mfrow = c(3, 2))
  on.exit(par(opar))
  pts <- barplot(object, main = main)
  if (!is.null(tag)) {
    gt <- as.vector(as.matrix(table(getClusters(object))))
    loc <- pts[round((c(0, cumsum(gt))[-(1 + length(gt))] + cumsum(gt))/2)]
    mtext(tag, side =1, line = 0, at = loc, col = object@palette, font = 2)
  }
  plot(object, view = "tsne", main = "t-SNE")
  #plot(object, view = "som")
  #plot(object, view = "umap", main = "UMAP")
  plot(object, view = "hclust")
  plot(object, view = "mds", main = "MDS")
}
```

Creating visuals using PAM method as default
```{r PAM, fig.width = 8, fig.height = 12, fig.cap = "PAM k = 6."}
f <- "simTsneNoise6K50nB1000S.Rda"
if (file.exists(f)) {
  load(f)
} else {
  set.seed(1987)
  vis <- Mercator(dis, "euclid", "hclust", K = 6)
  palette <- vis@palette[c(1:3, 7, 8, 6, 10, 4, 11, 5, 15, 14, 17:18, 9, 12, 16, 19:24)]
  vis@palette <- palette
  vis <- addVisualization(vis, "mds")
  vis <- addVisualization(vis, "tsne")
  #vis <- addVisualization(vis, "umap")
  #vis <- addVisualization(vis, "som", 
  #                          grid = kohonen::somgrid(8, 6, "hexagonal"))
  save(vis, palette, file = f)
}
rm(f)
```

Creating visual for what is considered true
```{r truebin, fig.width = 8, fig.height = 12, fig.cap = "Visualization of true cancer clusters."}
truebin <- setClusters(vis, as.numeric(labels))
truebin@palette <- palette
mercViews(truebin, main = "True Cluster Types", tag = unique(sort(labels)))
```

Looking at PAM clustering again, but also with the adjusted rand index value
```{r PAM2, fig.width = 8, fig.height = 12, fig.cap = "PAM k = 6."}
mercViews(vis, main = "PAM cluster, K = 6", tag = paste("C", 1:6, sep = ""))
adjustedRandIndex(truebin@clusters, vis@clusters)
```
Next, we applied Silly Putty to PAM to see if it helps get a higher mean of silhouette width and adjusted rand index
```{r sillypam, fig.width = 8, fig.height = 12, fig.cap = "SillyPutty PAM k = 6."}
pamSilly <- SillyPutty(vis@clusters, vis@distance)
pmSillyBin <- setClusters(vis, pamSilly@cluster)
pmSillyBin@palette <- palette
mercViews(pmSillyBin, main = "SillyPutty PAM Cluster Types", tag = paste("C", 1:6, sep = ""))
adjustedRandIndex(truebin@clusters, pmSillyBin@clusters)

```

#Hierarchical Clustering
Visualizing hierarchical clustering using the view "hclust" obtained from applying mercator to the dataset
```{r hk6, fig.width = 8, fig.height = 12, fig.cap = "hclust k = 6."}
hclass6 <- cutree(vis@view[["hclust"]], k = 6)
hk6 <- setClusters(vis, hclass6)
mercViews(hk6, main = "hk = 6",tag = paste("C", 1:6, sep = ""))

adjustedRandIndex(truebin@clusters, hclass6)
```
Then I applied SillyPutty to Hierarchical Clustering
```{r sillyhk6, fig.width = 8, fig.height = 12, fig.cap = "SillyPutty hclust k = 6."}
hcSilly <- SillyPutty(hk6@clusters, hk6@distance)
hcSillyBin <- setClusters(vis, hcSilly@cluster)
hcSillyBin@palette <- palette
mercViews(hcSillyBin, main = "SillyPutty HC Cluster Types", tag = paste("C", 1:6, sep = ""))
adjustedRandIndex(truebin@clusters, hcSillyBin@clusters)

```
We then tested how well Clara would perform on the dataset 
##Clara
```{r clara, fig.width = 8, fig.height = 12, fig.cap = "Clara K = 6."}
set.seed(17272)
cl <- clara(tdis,6,metric=c("euclidean"))
clVis <- setClusters(vis, cl$clustering)
mercViews(clVis, main = "Clara = 6")

adjustedRandIndex(truebin@clusters, clVis@clusters)

```
And then we applied sillyPutty to it
```{r sillyClara, fig.width = 8, fig.height = 12, fig.cap = "SillyPutty Clara K = 6."}
clSilly <- SillyPutty(clVis@clusters, dis)
clSillyBin <- setClusters(vis, clSilly@cluster)
clSillyBin@palette <- palette
mercViews(clSillyBin, main = "SillyPutty Clara Cluster Types", tag = paste("C", 1:6, sep = ""))
adjustedRandIndex(truebin@clusters, clSillyBin@clusters)

```
Lastly, we used kmeans clustering to cluster our data
```{r Kmeans, fig.width = 8, fig.height = 12, fig.cap = "Kmeans K = 6."}
set.seed(17272)
km <- kmeans(tdis,6)
ktypes <- setClusters(vis, as.numeric(km$cluster))
ktypes@palette <- palette
mercViews(ktypes, main = "KMEANS clustering, Clusters = 6", tag = paste("C", 1:6, sep = ""))

adjustedRandIndex(truebin@clusters, km$cluster)
```
And then we applied SillyPutty to kmeans clustering
```{r sillyKmeans, fig.width = 8, fig.height = 12, fig.cap = "SillyPutty Kmeans K = 6."}
kmeanSilly <- SillyPutty(km$cluster, dis)
kmSillyBin <- setClusters(vis, kmeanSilly@cluster)
kmSillyBin@palette <- palette
mercViews(kmSillyBin, main = "SillyPutty Kmeans Cluster Types", tag = paste("C", 1:6, sep = ""))
adjustedRandIndex(truebin@clusters, kmSillyBin@clusters)

```

```{r RandomSilly, fig.width = 8, fig.height = 12, fig.cap = "randomSilly K = 6."}
f <- "randSilly6k50nB1000S51514Seed100.Rda"
if (file.exists(f)) {
  load(f)
} else {
  set.seed(51514)
  y <- RandomSillyPutty(dis, 6, N = 100)
  ari.max <- adjustedRandIndex(truebin@clusters, y@MX)
  ari.min <- adjustedRandIndex(truebin@clusters, y@MN)
  randSillyBin <- setClusters(vis, y@MX)
  randSillyBin@palette <- palette
  save(y, ari.max, ari.min, randSillyBin, file = f)
}
mercViews(randSillyBin, main = "SillyPutty RandomSilly Cluster Types", tag = paste("C", 1:6, sep = ""))

```

# Appendix
This analysis was performed in this environment:
```{r si}
sessionInfo()
```
