---
author: Kevin R. Coombes
date: 9 November 2018
title: Apoptosis/Info
---

# Overview
This folder is **not** part of our current standard structure (but we
may want to something like it to the standard eventually.

The purpose mof te folder is to hold relatively small, text-based data
files that we are currently viewing as "core information" needed for
some portion of the analysis. Notmonly are the small and text-based
(which makes them reasonable to include in a git repository), but it
is also conceivable that we might want to change them in the future.

For this project the two (related) examples of such files are:

1. `apop00.graphml` contains a "GraphML" represntation of the
   apoptosis pathway. The format being used is the native format of
   the [yEd Graph Editor](https://www.yworks.com/products/yed) with
   which it was produced. Docuemntation of the format can be fouind at
   the same URL.
2. `genes00.txt` contains a list of the genes (by symbol) in the
   graph. It was extracted long ago during the prehistory of this
   project. But this format is much more useful for linking to other
   data sources.
