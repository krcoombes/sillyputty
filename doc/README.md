---
author: Kevin R, Coombes
date: 9 November 2018
title: Apoptosis/Doc
---

# Overview
This folder contains documentation related to the project. In a pure
programming project, that would mean the obvious thing: manual pages,
R/BioConductor vignettes, etc. In a bioinformatics or computational
biology project, however, it is more likely to mean papers (articles),
grant proposals, or presentations (talks or posters). We predefine two
subfolders:

1. _paper_, to hold the source material of an article intended for
   publication, and
2. _presentation_, to hold the source material for a talk about the
   research.

Sometimes we make sub-subfolders for mutliple related papers, talks,
posters, etc. Other times, we create additional top-level folders. Use
whichever method works for you.

# Changes:

This folder used to contain a subfolder called "Vignette". It contained
a draft vignette for the associated R package. It has now been
incorporated into the package. As a result, it has been deleted from the
Git project.

It also used to contain a subfolder called "man", which contained
initial drafts of manual pages for the R package. We have moveed those
files into the package, and deleted them from the Git project.
