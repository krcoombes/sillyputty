---
author: Kevin R, Coombes
date: 9 November 2018
title: /Doc/Paper
---

# Overview
This folder contains the source material for articles describing the
research project and intended for publication.

## Thresher
One of our early ideas for applicatiosn of Thresher was to decompose
the apoptosis pathway into "_pathways building blocks_" (PBBs) or
"_biological components_". We use those terms interchangably to refer
to our replacement of the mathematical principal components with
something that is biologically interpretable.

## SillyPutty
While trying to repeat/refine/improve Min Wang's original application
fo Thresher to the apoptosis data set, I had an idea for a new
clusteing algorithm (currently called `SillyPutty`). The first
applications of the method are to understand the apoptosis data, and
we expect that this will remain a significant application when we
eventually get around to publishing the method.

# Track Changes
For the resubmission at PLoS ONE, the editor wanted a manuscript that
marked the changes (like MS Word's "track changes" mode). Since our
manuscript was prepared using Rmarkdown in RStuduio, this task was a
bit of a challenge. Here, we document the steps required.

1. First, we need to locate a copy of the Rmarkdown source of the
   file that was originally submitted. If we had been smarter, we
   would have used a git "tag" to record that version. (In the future,
   we wil be smarter.) Since we didn't do that, I used the Git "Show
   log" optionn from the TortoiseGit installation on my Windows PC,
   and was able to figure out (partly from the commit comments) which
   commit conained the submitted version. Note that this is identified
   by a long string of random-numbers-and-letters which is the
   Sha-hash identifying the comimit.
2. Now, in a git bash window, enter a command that look something like
   `git show [sha-hash]:[file name > out.Rmd`. Here you have to replace
   [sha-hash] by copy-and-pasting the commit hash value. And you have
   to replace [file-name] by the (surprise!) actual file name. In this
   case, that was `PLOS_SillyPutty.Rmd`. Note that this step does not
   require us to revert the file to the old version, but it does
   require us to save the recovered file with a different name.
3. Use RStudio to knit both the current Rmd file (`PLOS_SIlly_Putty`)
   and the recovered version (`old`) to PDF files. However, you should
   also make sure that the YAML preamnle in both Rmd file includes the
   option `keep_tex: true` so that the (intermediate) LaTeX files are
   saved.
4. Now we can use the `latexdiff` pacakge from the Comprehensive TeX
   Archive Network (CTAN). On a Windows PC, this is automatically
   installed when you install MikTeX. I'm not sure if ti comes along
   with the Mac TeX installations. Now, `latexdiff` is actually a perl
   sxcript, so you might also have to install a version of perl to get
   it to work. (I always install Strawberry perl on my Windows boxes.)
5. The appropriate command is something like
   `latexdiff -t UNDERLINE old.tex PLOS_Silly_Putty_tex > markchanges.tex`
   This command will crete yet another LaTeX file that indicates what
   edits were made. The `UNDERLINE` part says how you want insertions
   and deletions colored and marked; read the latexdiff manual to see
   what other options are available. Or don't, and just use this one.
6. Now produce a PDf file from the version with marked changes by
   `pdflatex markchanges` (which creates `markchanges.pdf`).
