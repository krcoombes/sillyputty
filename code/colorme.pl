#! perl -w

use strict;
use warnings;
use XML::Twig;

my $colors = shift || 'genes.txt';
my $input  = shift || 'input.graphml';
my $output = shift || 'output.graphml';

print STDERR "Using '$colors' to color '$input' and write it to '$output'.\n";

my %colorhash = ();
open(COLORS, "<$colors") or die "Cannot find '$colors': $!\n";
while (my $line = <COLORS>) {
    chomp $line;
    my ($gene, $color) = split /\t/, $line;
    $colorhash{$gene} = $color;
}
close(COLORS);

open(my $fh, ">$output") or die "Unable to create '$output': $!\n";

my $twig = XML::Twig->new(
    twig_roots => {'y:ShapeNode' => \&convert},
    twig_print_outside_roots => $fh
);
$twig->parsefile($input);


close($fh);

sub convert {
    my ($twig, $nl) = @_;
    my $gene = $nl->first_child('y:NodeLabel');
    my $txt = $gene->text;
    if (defined($colorhash{$txt})) {
	my $fill = $nl->first_child('y:Fill');
	my $ccol = $fill->att('color');
	my $color = $colorhash{$txt};
	$fill->set_att( 'color' => $color );
	print STDERR $txt, "\t$ccol => $color\n";
    }
    $nl->print($fh);
}

exit;
__END__
