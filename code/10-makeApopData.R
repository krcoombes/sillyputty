###########################################
source("00-paths.R")

f <- file.path(paths$clean, "apopData.Rda")
if (file.exists(f)) {
  load(f)
} else {
  ## Find the list of apoptosis related genes.
  geneset <- read.table("../info/genes00.txt", header=FALSE,
                        sep="\t", comment.char="")
  geneNames <- as.character(geneset$V1)
  ## Get the TCGA mRNA-Seq data.
  load(file.path(TCGApaths$clean , "mRNAdata.Rda"))
  if (!all(geneNames %in% rownames(mRNAdata))) {
    stop("Bad geneNames.")
  }
  ## Focus on the apoptosis genes.
  apopData <- mRNAdata[geneNames,]
  ## Get the cohort information
  load(file.path(TCGApaths$clean , "cohorts.Rda"))
  ## remove stuff we don't need
  rm(mRNAdata, tumorlist, geneset, geneNames)
  rm(tcgaMeta, cohorts, nzero, idLists)
  ## save the apoptosis data
  save(apopData, cancerType, cohortColors, file = f)
  ## memory cleanup (garbage collection)
  gc()
}
rm(f)
