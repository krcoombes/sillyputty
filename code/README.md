---
author: Kevin R. Coombes
date:  17 June 2019
title: SillyPutty/Code
---

# Overview
This folder contains the computer code used to perform all analyses
related to the project. Here are brief descriptions of the main pieces,

# 00-paths.R
This script implements our standard scheme that allows people to store
copies of the data in different places on different machines, while
still making sure all of the code runs. For the SillyPutty project, it
depends on two other (JSON) files that describe the "paths" to the
data.

These files must be stored in a specific location on your computer in
order for the `00-paths.R` script to be able to find them. First, they
have to be inside your "home" directory, which is symbolized by `$HOME`
below. 

On LINUX or Mac computers, this is already defined. On a
Windows computer, you probably have to define an environment variable
called `HOME`.  The easist way to do this is probably to
`set HOME=C:/Users/[you]/Documents`
in a way that your computer will remember.

Inside the home directory, the JSON have to be stored in a
subdirectory called `Paths`.(Note that these rules are just a
_convention_ that we have adopted to make things work. Some piece of
information has to be shared between machines for this scheme ot work,
so we have agreed that every machine must put its path-defining JSON
files in `$HOME/Paths`.

## $HOME/Paths/tcga.json
The `tcga.json` file comes from a related projected that collects data
from 
[The Cancer Genome Atlas](https://www.cancer.gov/about-nci/organization/ccg/research/structural-genomics/tcga).
(Well, actually, it mostly gets it from
[FireBrowse](http://firebrowse.org/), but that's a whole nother
story.) It does illustrate another convention that we have
adoppted. We mostly define three separate paths:

* one for `raw` data that we obtained directlry fromthe source,
* one for `clean` or processed data, and
* `scratch` space for things we want to save along the way.

On my work machine, the "P" drive is a network drive that the
uinversity backs up regularly, so I use it for my data:

```json
{
  "paths" : {
      "raw"     : "P:/TCGA/raw",
      "clean"   : "P:/TCGA/clean",
      "scratch" : "P:/TCGA/scratch"
  }
}
```

## $HOME/Paths/silly.json
The `silly.json` file defines the same kinds of paths that are
specifically for the SillyPutty project.

```json
{
  "paths" : {
      "raw"     : "P:/Projects/Apoptosis/Raw",
      "clean"   : "P:/Projects/Apoptosis/Clean",
      "scratch" : "P:/Projects/Apoptosis/Scratch"
  }
}
```

# 10-makeApopData.R
The only point of this script is to creates a "clean" data set
containing the expression of 119 apoptosis-related genes in more than
10,000 patient samples from TCGA. These dataa re saved in a file
called `apopData.Rda`.

# 100-apoptosis.R
This script is part of a different project (to better understand
apoptosis geens) that is currently languishing.

# 200-sillyPutty.Rmd
Thsi Rmarkdown file is the main thing we want to use. It tests the
SillyPutty clustering algorithm on the apoptosis data to try to
cluster the genes.
